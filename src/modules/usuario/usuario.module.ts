import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Persona } from 'src/common/entities/Persona';
import { Rol } from 'src/common/entities/Rol';
import { Usuario } from 'src/common/entities/Usuario';
import { UsuarioRol } from 'src/common/entities/UsuarioRol';
import { UsuarioController } from './usuario.controller';
import { UsuarioService } from './usuario.service';

@Module({
  imports: [TypeOrmModule.forFeature([Usuario, Persona, Rol, UsuarioRol])],
  controllers: [UsuarioController],
  providers: [UsuarioService],
  exports: [UsuarioService], // Es necesario exporta UsersService para que se pueda utilizar en AuthService
})
export class UsuarioModule {}
