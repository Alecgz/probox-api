import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Persona } from 'src/common/entities/Persona';
import { Rol } from 'src/common/entities/Rol';
import { Usuario } from 'src/common/entities/Usuario';
import { UsuarioRol } from 'src/common/entities/UsuarioRol';
import { Repository } from 'typeorm';
import { CreateUsuarioDto } from './dto/create-usuario.dto';
import { UpdateUsuarioDto } from './dto/update-usuario.dto';

@Injectable()
export class UsuarioService {
  constructor(
    @InjectRepository(Usuario)
    private readonly _usuarioRepository: Repository<Usuario>,
    @InjectRepository(Persona)
    private readonly _personaRepository: Repository<Persona>,
    @InjectRepository(Rol)
    private readonly _rolRepository: Repository<Rol>,
    @InjectRepository(UsuarioRol)
    private readonly _usuarioRolRepository: Repository<UsuarioRol>,
  ) {}

  async create(createUsuarioDto: CreateUsuarioDto): Promise<Usuario> {
    const defaultRole = await this._rolRepository.findOne({
      where: { nombre: 'Profesor' },
    });

    const persona = await this._personaRepository.findOne(
      createUsuarioDto.personaId,
    );

    if (!persona) {
      throw new NotFoundException('Resource persona not found');
    }

    const entity: Usuario = this._usuarioRepository.create({
      ...createUsuarioDto,
      persona,
    });

    const savedUser: Usuario = await this._usuarioRepository.save(entity);

    const rolesAssigned = await this._usuarioRolRepository.create({
      rol: defaultRole,
      usuario: savedUser,
    });

    await this._usuarioRolRepository.save(rolesAssigned);

    return savedUser;
  }

  async findAll(): Promise<Usuario[]> {
    return await this._usuarioRepository.find({
      relations: ['persona', 'persona.genero', 'persona.tipodoc'],
    });
  }

  findOne(id: number) {
    // : Promise<User | undefined>
    return `This action returns a #${id} usuario`;
  }

  update(id: number, updateUsuarioDto: UpdateUsuarioDto) {
    return `This action updates a #${id} usuario`;
  }

  remove(id: number) {
    return `This action removes a #${id} usuario`;
  }

  async getUserByEmailOrUsername(username?: string): Promise<Usuario> {
    return await this._usuarioRepository.findOne({
      where: [{ username }],
    });
  }
}
