import { Genero } from 'src/common/entities/Genero';
import { Persona } from 'src/common/entities/Persona';
import { Rol } from 'src/common/entities/Rol';
import { TipoDoc } from 'src/common/entities/TipoDoc';
import { Usuario } from 'src/common/entities/Usuario';
import { MigrationInterface, QueryRunner } from 'typeorm';

export class seed2238964632000 implements MigrationInterface {
  name = 'seed2238964632000';

  public async up(queryRunner: QueryRunner): Promise<void> {
    // Agregando los Géneros
    const generoMasculino = await queryRunner.manager.save(
      queryRunner.manager.create<Genero>(Genero, {
        id: 1,
        nombre: 'Masculino',
        sigla: 'M',
      }),
    );
    await queryRunner.manager.save(
      queryRunner.manager.create<Genero>(Genero, {
        id: 2,
        nombre: 'Femenino',
        sigla: 'F',
      }),
    );

    // Agregando los Tipos de Documento
    const tipoDocDNI = await queryRunner.manager.save(
      queryRunner.manager.create<TipoDoc>(TipoDoc, {
        id: 1,
        nombre: 'DNI',
        minLong: 8,
        maxLong: 8,
        validacionMask: '',
      }),
    );
    await queryRunner.manager.save(
      queryRunner.manager.create<TipoDoc>(TipoDoc, {
        id: 2,
        nombre: 'Carnet de extranjería',
        minLong: 12,
        maxLong: 12,
        validacionMask: '',
      }),
    );
    await queryRunner.manager.save(
      queryRunner.manager.create<TipoDoc>(TipoDoc, {
        id: 3,
        nombre: 'RUC',
        minLong: 11,
        maxLong: 11,
        validacionMask: '',
      }),
    );
    await queryRunner.manager.save(
      queryRunner.manager.create<TipoDoc>(TipoDoc, {
        id: 4,
        nombre: 'Pasaporte',
        minLong: 12,
        maxLong: 12,
        validacionMask: '',
      }),
    );

    // Agregando los Roles
    await queryRunner.manager.save(
      queryRunner.manager.create<Rol>(Rol, {
        id: 1,
        nombre: 'Administrador',
      }),
    );
    await queryRunner.manager.save(
      queryRunner.manager.create<Rol>(Rol, {
        id: 2,
        nombre: 'Profesor',
      }),
    );
    await queryRunner.manager.save(
      queryRunner.manager.create<Rol>(Rol, {
        id: 3,
        nombre: 'Alumno',
      }),
    );

    // Agregando datos del Owner
    const personaOwner = await queryRunner.manager.save(
      queryRunner.manager.create<Persona>(Persona, {
        nroDocumento: '70500954',
        nombres: 'Alec',
        apePaterno: 'González',
        apeMaterno: 'Coral',
        fecNacimiento: '01-03-1994',
        direccion: '',
        celular: '',
        telefono: '',
        correo: '',
        foto: '',
        genero: generoMasculino,
        tipodoc: tipoDocDNI,
      }),
    );

    // Agregando datos del Usuario
    await queryRunner.manager.save(
      queryRunner.manager.create<Usuario>(Usuario, {
        username: 'agonzalez',
        password: 'admin123456',
        activo: true,
        persona: personaOwner,
      }),
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`DELETE FROM usuario`);
    await queryRunner.query(`DELETE FROM persona`);

    await queryRunner.query(`DELETE FROM tipo_doc`);
    await queryRunner.query(`DELETE FROM genero`);
    await queryRunner.query(`DELETE FROM rol`);
  }
}
