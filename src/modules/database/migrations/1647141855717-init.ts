import { MigrationInterface, QueryRunner } from 'typeorm';

export class init1647141855717 implements MigrationInterface {
  name = 'init1647141855717';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `CREATE TABLE "tipo_doc" ("id" integer NOT NULL, "nombre" character varying(50) NOT NULL, "min_long" integer NOT NULL, "max_long" integer NOT NULL, "validacion_mask" character varying(255) NOT NULL, CONSTRAINT "PK_91258cc39786c5c7fde66f31c46" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `CREATE UNIQUE INDEX "tipo_doc_pkey" ON "tipo_doc" ("id") `,
    );
    await queryRunner.query(
      `CREATE TABLE "rol" ("id" integer NOT NULL, "nombre" character varying(50) NOT NULL, CONSTRAINT "PK_c93a22388638fac311781c7f2dd" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(`CREATE UNIQUE INDEX "rol_pkey" ON "rol" ("id") `);
    await queryRunner.query(
      `CREATE TABLE "usuario_rol" ("id" SERIAL NOT NULL, "rol_id" integer, "usuario_id" integer, CONSTRAINT "PK_6c336b0a51b5c4d22614cb02533" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `CREATE UNIQUE INDEX "usuario_rol_pkey" ON "usuario_rol" ("id") `,
    );
    await queryRunner.query(
      `CREATE TABLE "usuario" ("id" SERIAL NOT NULL, "username" character varying(50) NOT NULL, "password" character varying(500) NOT NULL, "activo" boolean NOT NULL DEFAULT true, "persona_id" integer, CONSTRAINT "PK_a56c58e5cabaa04fb2c98d2d7e2" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `CREATE UNIQUE INDEX "usuario_pkey" ON "usuario" ("id") `,
    );
    await queryRunner.query(
      `CREATE TABLE "persona" ("id" SERIAL NOT NULL, "nro_documento" character varying(15) NOT NULL, "nombres" character varying(80) NOT NULL, "ape_paterno" character varying(80) NOT NULL, "ape_materno" character varying(80) NOT NULL, "fec_nacimiento" date NOT NULL, "direccion" character varying(300) NOT NULL, "celular" character varying(9) NOT NULL, "telefono" character varying(15) NOT NULL, "correo" character varying(150) NOT NULL, "foto" character varying(500) NOT NULL, "genero_id" integer, "tipodoc_id" integer, CONSTRAINT "PK_13aefc75f60510f2be4cd243d71" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `CREATE UNIQUE INDEX "persona_pkey" ON "persona" ("id") `,
    );
    await queryRunner.query(
      `CREATE TABLE "genero" ("id" integer NOT NULL, "nombre" character varying(50) NOT NULL, "sigla" character varying(255) NOT NULL, CONSTRAINT "PK_681c2c8d602304f33f9cc74e6ad" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `CREATE UNIQUE INDEX "genero_pkey" ON "genero" ("id") `,
    );
    await queryRunner.query(
      `CREATE TABLE "tuit" ("id" SERIAL NOT NULL, "message" character varying NOT NULL, "test" boolean NOT NULL, CONSTRAINT "PK_3359c56619d77bf7a62427924c8" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `ALTER TABLE "usuario_rol" ADD CONSTRAINT "FK_ac8911cd54a61461c9926541401" FOREIGN KEY ("rol_id") REFERENCES "rol"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "usuario_rol" ADD CONSTRAINT "FK_29e9a9079c7ba01c1b301cf5555" FOREIGN KEY ("usuario_id") REFERENCES "usuario"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "usuario" ADD CONSTRAINT "FK_c9d223fa9cc0ea30abcd9d5ca7e" FOREIGN KEY ("persona_id") REFERENCES "persona"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "persona" ADD CONSTRAINT "FK_b5035180a48a15df1e48dbc05b8" FOREIGN KEY ("genero_id") REFERENCES "genero"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "persona" ADD CONSTRAINT "FK_00bd87526c421d05ef462a0a70b" FOREIGN KEY ("tipodoc_id") REFERENCES "tipo_doc"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "persona" DROP CONSTRAINT "FK_00bd87526c421d05ef462a0a70b"`,
    );
    await queryRunner.query(
      `ALTER TABLE "persona" DROP CONSTRAINT "FK_b5035180a48a15df1e48dbc05b8"`,
    );
    await queryRunner.query(
      `ALTER TABLE "usuario" DROP CONSTRAINT "FK_c9d223fa9cc0ea30abcd9d5ca7e"`,
    );
    await queryRunner.query(
      `ALTER TABLE "usuario_rol" DROP CONSTRAINT "FK_29e9a9079c7ba01c1b301cf5555"`,
    );
    await queryRunner.query(
      `ALTER TABLE "usuario_rol" DROP CONSTRAINT "FK_ac8911cd54a61461c9926541401"`,
    );
    await queryRunner.query(`DROP TABLE "tuit"`);
    await queryRunner.query(`DROP INDEX "public"."genero_pkey"`);
    await queryRunner.query(`DROP TABLE "genero"`);
    await queryRunner.query(`DROP INDEX "public"."persona_pkey"`);
    await queryRunner.query(`DROP TABLE "persona"`);
    await queryRunner.query(`DROP INDEX "public"."usuario_pkey"`);
    await queryRunner.query(`DROP TABLE "usuario"`);
    await queryRunner.query(`DROP INDEX "public"."usuario_rol_pkey"`);
    await queryRunner.query(`DROP TABLE "usuario_rol"`);
    await queryRunner.query(`DROP INDEX "public"."rol_pkey"`);
    await queryRunner.query(`DROP TABLE "rol"`);
    await queryRunner.query(`DROP INDEX "public"."tipo_doc_pkey"`);
    await queryRunner.query(`DROP TABLE "tipo_doc"`);
  }
}
