import { Body, Controller, Get, Post } from '@nestjs/common';
import { ApiOkResponse, ApiTags } from '@nestjs/swagger';
import { Reportes } from 'src/common/entities/Reportes';
import { CreateReporteDto } from './dto/create-reporte.dto';
import { ReporteService } from './reporte.service';

@ApiTags('reporte')
@Controller('reporte')
export class ReporteController {
  constructor(private readonly reporteService: ReporteService) {}

  @Post()
  create(@Body() createReporteDto: CreateReporteDto): Promise<Reportes> {
    return this.reporteService.create(createReporteDto);
  }

  @ApiOkResponse({ type: [Reportes] })
  @Get()
  findAll(): Promise<Reportes[]> {
    return this.reporteService.findAll();
  }
}
