import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Reportes } from 'src/common/entities/Reportes';
import { ReporteController } from './reporte.controller';
import { ReporteService } from './reporte.service';

@Module({
  imports: [TypeOrmModule.forFeature([Reportes])],
  controllers: [ReporteController],
  providers: [ReporteService],
})
export class ReporteModule {}
