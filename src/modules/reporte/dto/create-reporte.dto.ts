import { ApiProperty } from '@nestjs/swagger';
import { IsString } from 'class-validator';

export class CreateReporteDto {
  @ApiProperty()
  @IsString()
  readonly nombre: string;
}
