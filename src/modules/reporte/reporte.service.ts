import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Reportes } from 'src/common/entities/Reportes';
import { Repository } from 'typeorm';
import { CreateReporteDto } from './dto/create-reporte.dto';

@Injectable()
export class ReporteService {
  constructor(
    @InjectRepository(Reportes)
    private readonly reporteRepository: Repository<Reportes>,
  ) {}

  async create(createReporteDto: CreateReporteDto): Promise<Reportes> {
    const entity: Reportes = this.reporteRepository.create({
      ...createReporteDto,
    });
    return this.reporteRepository.save(entity);
  }

  async findAll(): Promise<Reportes[]> {
    return await this.reporteRepository.find();
  }
}
