import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Genero } from 'src/common/entities/Genero';
import { Persona } from 'src/common/entities/Persona';
import { TipoDoc } from 'src/common/entities/TipoDoc';
import { PersonaController } from './persona.controller';
import { PersonaService } from './persona.service';

@Module({
  imports: [TypeOrmModule.forFeature([Persona, Genero, TipoDoc])],
  controllers: [PersonaController],
  providers: [PersonaService],
})
export class PersonaModule {}
