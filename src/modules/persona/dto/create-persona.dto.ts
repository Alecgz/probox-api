import { ApiProperty } from '@nestjs/swagger';
import { IsNumber, IsString } from 'class-validator';

export class CreatePersonaDto {
  @ApiProperty()
  @IsString()
  readonly nroDocumento: string;

  @ApiProperty()
  @IsString()
  readonly nombres: string;

  @ApiProperty()
  @IsString()
  readonly apePaterno: string;

  @ApiProperty()
  @IsString()
  readonly apeMaterno: string;

  @ApiProperty()
  @IsString()
  readonly fecNacimiento: string;

  @ApiProperty()
  @IsString()
  readonly direccion: string;

  @ApiProperty()
  @IsString()
  readonly celular: string;

  @ApiProperty()
  @IsString()
  readonly telefono: string;

  @ApiProperty()
  @IsString()
  readonly correo: string;

  @ApiProperty()
  @IsString()
  readonly foto: string;

  @ApiProperty()
  @IsNumber()
  readonly generoId: number;

  @ApiProperty()
  @IsNumber()
  readonly tipodocId: number;
}
