import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Genero } from 'src/common/entities/Genero';
import { Persona } from 'src/common/entities/Persona';
import { TipoDoc } from 'src/common/entities/TipoDoc';
import { Repository } from 'typeorm';
import { CreatePersonaDto } from './dto/create-persona.dto';
import { UpdatePersonaDto } from './dto/update-persona.dto';

@Injectable()
export class PersonaService {
  constructor(
    @InjectRepository(Persona)
    private readonly personaRepository: Repository<Persona>,

    @InjectRepository(Genero)
    private readonly generoRepository: Repository<Genero>,

    @InjectRepository(TipoDoc)
    private readonly tipoDocRepository: Repository<TipoDoc>,
  ) {}

  async create(createPersonaDto: CreatePersonaDto): Promise<Persona> {
    const genero = await this.generoRepository.findOne(
      createPersonaDto.generoId,
    );

    if (!genero) {
      throw new NotFoundException('Resource genero not found');
    }

    const tipodoc = await this.tipoDocRepository.findOne(
      createPersonaDto.tipodocId,
    );

    if (!tipodoc) {
      throw new NotFoundException('Resource tipoDoc not found');
    }

    const entity: Persona = this.personaRepository.create({
      ...createPersonaDto,
      genero,
      tipodoc,
    });
    return this.personaRepository.save(entity);
  }

  async findAll(): Promise<Persona[]> {
    return await this.personaRepository.find({
      relations: ['genero', 'tipodoc'],
    });
  }

  findOne(id: number) {
    return `This action returns a #${id} persona`;
  }

  async update(id: number, updatePersonaDto: UpdatePersonaDto) {
    return `This action updates a #${id} persona`;
  }

  remove(id: number) {
    return `This action removes a #${id} persona`;
  }
}
