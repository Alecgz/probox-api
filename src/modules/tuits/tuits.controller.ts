import { Controller, Get } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { TuitsService } from './tuits.service';

@ApiTags('tuits')
@Controller('tuits')
export class TuitsController {
  constructor(private readonly tuitsService: TuitsService) {}

  @Get()
  getSaludo() {
    return 'hola';
  }
}
