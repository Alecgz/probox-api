import { ValidationPipe } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { AppModule } from './app.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  const apiDescription = new DocumentBuilder()
    .setTitle('Probox API')
    .setDescription('Documentacion de los servicios del API Probox')
    .setVersion('1.0')
    .build();

  const document = SwaggerModule.createDocument(app, apiDescription, {});
  SwaggerModule.setup('api/docs', app, document, {
    explorer: true,
    swaggerOptions: {
      filter: true,
      docExpansion: 'none',
      tagsSorter: 'alpha',
    },
  });

  app.useGlobalPipes(
    new ValidationPipe({
      transform: true,
      whitelist: true,
      forbidNonWhitelisted: true,
      transformOptions: {
        enableImplicitConversion: true,
      },
    }),
  );

  // app.useGlobalFilters(new AllExceptionsFilter());

  await app.listen(AppModule.port);
}
bootstrap();
