import { HttpException, HttpStatus } from '@nestjs/common';

export class NohayException extends HttpException {
  constructor() {
    super('No hay', HttpStatus.NOT_FOUND);
  }
}
