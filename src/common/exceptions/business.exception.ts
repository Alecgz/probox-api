import { HttpException, HttpStatus } from '@nestjs/common';

export class BusinessException extends HttpException {
  constructor() {
    super('BusinessException has ocurred!', HttpStatus.NOT_FOUND);
  }
}
