import { Column, Entity, Index, PrimaryGeneratedColumn } from 'typeorm';

@Index('reportes_pkey', ['id'], { unique: true })
@Entity('reportes', { schema: 'public' })
export class Reportes {
  @PrimaryGeneratedColumn({ type: 'integer', name: 'id' })
  id: number;

  @Column('character varying', { name: 'nombre', nullable: true, length: 255 })
  nombre: string | null;
}
