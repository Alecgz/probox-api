import { ApiProperty } from '@nestjs/swagger';
import { Column, Entity, Index, OneToMany } from 'typeorm';
import { Persona } from './Persona';

@Index('genero_pkey', ['id'], { unique: true })
@Entity('genero', { schema: 'public' })
export class Genero {
  @ApiProperty()
  @Column('integer', { primary: true, name: 'id' })
  id: number;

  @ApiProperty()
  @Column('character varying', { name: 'nombre', length: 50 })
  nombre: string;

  @ApiProperty()
  @Column('character varying', { name: 'sigla', length: 255 })
  sigla: string;

  @OneToMany(() => Persona, (persona) => persona.genero)
  personas: Persona[];
}
