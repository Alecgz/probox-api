import { Column, Entity, Index, OneToMany } from 'typeorm';
import { UsuarioRol } from './UsuarioRol';

@Index('rol_pkey', ['id'], { unique: true })
@Entity('rol', { schema: 'public' })
export class Rol {
  @Column('integer', { primary: true, name: 'id' })
  id: number;

  @Column('character varying', { name: 'nombre', length: 50 })
  nombre: string;

  @OneToMany(() => UsuarioRol, (usuarioRol) => usuarioRol.rol)
  usuarioRols: UsuarioRol[];
}
