import { ApiProperty } from '@nestjs/swagger';
import {
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Genero } from './Genero';
import { TipoDoc } from './TipoDoc';
import { Usuario } from './Usuario';

@Index('persona_pkey', ['id'], { unique: true })
@Entity('persona', { schema: 'public' })
export class Persona {
  @ApiProperty()
  @PrimaryGeneratedColumn({ type: 'integer', name: 'id' })
  id: number;

  @ApiProperty()
  @Column('character varying', { name: 'nro_documento', length: 15 })
  nroDocumento: string;

  @ApiProperty()
  @Column('character varying', { name: 'nombres', length: 80 })
  nombres: string;

  @ApiProperty()
  @Column('character varying', { name: 'ape_paterno', length: 80 })
  apePaterno: string;

  @ApiProperty()
  @Column('character varying', { name: 'ape_materno', length: 80 })
  apeMaterno: string;

  @ApiProperty()
  @Column('date', { name: 'fec_nacimiento' })
  fecNacimiento: string;

  @ApiProperty()
  @Column('character varying', { name: 'direccion', length: 300 })
  direccion: string;

  @ApiProperty()
  @Column('character varying', { name: 'celular', length: 9 })
  celular: string;

  @ApiProperty()
  @Column('character varying', { name: 'telefono', length: 15 })
  telefono: string;

  @ApiProperty()
  @Column('character varying', { name: 'correo', length: 150 })
  correo: string;

  @ApiProperty()
  @Column('character varying', { name: 'foto', length: 500 })
  foto: string;

  @ApiProperty({ required: false, type: () => Genero })
  @ManyToOne(() => Genero, (genero) => genero.personas)
  @JoinColumn([{ name: 'genero_id', referencedColumnName: 'id' }])
  genero: Genero;

  @ApiProperty({ required: false })
  @ManyToOne(() => TipoDoc, (tipoDoc) => tipoDoc.personas)
  @JoinColumn([{ name: 'tipodoc_id', referencedColumnName: 'id' }])
  tipodoc: TipoDoc;

  @OneToMany(() => Usuario, (usuario) => usuario.persona)
  usuarios: Usuario[];
}
