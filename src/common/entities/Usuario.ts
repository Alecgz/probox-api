import { ApiProperty } from '@nestjs/swagger';
import { genSalt, hash } from 'bcrypt';
import {
  BeforeInsert,
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Persona } from './Persona';
import { UsuarioRol } from './UsuarioRol';

@Index('usuario_pkey', ['id'], { unique: true })
@Entity('usuario', { schema: 'public' })
export class Usuario {
  @ApiProperty()
  @PrimaryGeneratedColumn({ type: 'integer', name: 'id' })
  id: number;

  @ApiProperty()
  @Column('character varying', { name: 'username', length: 50 })
  username: string;

  @ApiProperty()
  @Column('character varying', { name: 'password', length: 500 })
  password: string;

  @ApiProperty()
  @Column('boolean', { name: 'activo', default: () => 'true' })
  activo: boolean;

  @ApiProperty({ type: () => Persona })
  @ManyToOne(() => Persona, (persona) => persona.usuarios)
  @JoinColumn([{ name: 'persona_id', referencedColumnName: 'id' }])
  persona: Persona;

  @OneToMany(() => UsuarioRol, (usuarioRol) => usuarioRol.usuario)
  usuarioRols: UsuarioRol[];

  @BeforeInsert()
  async hashPassword() {
    const salt = await genSalt(8);
    this.password = await hash(this.password, salt);
  }
}
