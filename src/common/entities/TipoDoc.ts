import { ApiProperty } from '@nestjs/swagger';
import { Column, Entity, Index, OneToMany } from 'typeorm';
import { Persona } from './Persona';

@Index('tipo_doc_pkey', ['id'], { unique: true })
@Entity('tipo_doc', { schema: 'public' })
export class TipoDoc {
  @ApiProperty()
  @Column('integer', { primary: true, name: 'id' })
  id: number;

  @ApiProperty()
  @Column('character varying', { name: 'nombre', length: 50 })
  nombre: string;

  @ApiProperty()
  @Column('integer', { name: 'min_long' })
  minLong: number;

  @ApiProperty()
  @Column('integer', { name: 'max_long' })
  maxLong: number;

  @ApiProperty()
  @Column('character varying', { name: 'validacion_mask', length: 255 })
  validacionMask: string;

  @OneToMany(() => Persona, (persona) => persona.tipodoc)
  personas: Persona[];
}
