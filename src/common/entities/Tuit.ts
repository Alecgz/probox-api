import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity('tuit', { schema: 'public' })
export class Tuit {
  @PrimaryGeneratedColumn({ type: 'integer', name: 'id' })
  id: number;

  @Column('character varying', { name: 'message' })
  message: string;

  @Column('boolean', { name: 'test' })
  test: boolean;
}
