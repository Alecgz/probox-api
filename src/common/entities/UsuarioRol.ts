import {
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Rol } from './Rol';
import { Usuario } from './Usuario';

@Index('usuario_rol_pkey', ['id'], { unique: true })
@Entity('usuario_rol', { schema: 'public' })
export class UsuarioRol {
  @PrimaryGeneratedColumn({ type: 'integer', name: 'id' })
  id: number;

  @ManyToOne(() => Rol, (rol) => rol.usuarioRols)
  @JoinColumn([{ name: 'rol_id', referencedColumnName: 'id' }])
  rol: Rol;

  @ManyToOne(() => Usuario, (usuario) => usuario.usuarioRols)
  @JoinColumn([{ name: 'usuario_id', referencedColumnName: 'id' }])
  usuario: Usuario;
}
