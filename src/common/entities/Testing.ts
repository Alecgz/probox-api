import { Column, Entity, Index, PrimaryGeneratedColumn } from 'typeorm';

@Index('testing_pkey', ['id'], { unique: true })
@Entity('testing', { schema: 'public' })
export class Testing {
  @PrimaryGeneratedColumn({ type: 'integer', name: 'id' })
  id: number;

  @Column('character varying', { name: 'nombre', length: 255 })
  nombre: string;

  @Column('character varying', { name: 'password', length: 255 })
  password: string;
}
