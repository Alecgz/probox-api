import {
  ArgumentsHost,
  BadRequestException,
  Catch,
  ExceptionFilter,
  HttpException,
  HttpStatus,
} from '@nestjs/common';
import { HttpAdapterHost } from '@nestjs/core';
import { camelToSnakeCase } from '../utils/utils';

@Catch()
export class AllExceptionsFilter implements ExceptionFilter {
  constructor(private readonly httpAdapterHost: HttpAdapterHost) {}

  catch(exception: unknown, host: ArgumentsHost): void {
    // In certain situations `httpAdapter` might not be available in the
    // constructor method, thus we should resolve it here.
    const { httpAdapter } = this.httpAdapterHost;

    const ctx = host.switchToHttp();

    const httpStatus =
      exception instanceof HttpException
        ? exception.getStatus()
        : HttpStatus.INTERNAL_SERVER_ERROR;

    let validationErrors = [];
    if (exception instanceof BadRequestException) {
      const badReqResp =
        exception instanceof BadRequestException
          ? exception.getResponse()
          : null;
      if (badReqResp != null) {
        const badReqMessage = badReqResp['message'];
        if (badReqMessage.constructor.name == 'Array') {
          validationErrors = badReqMessage;
        }
      }
    }

    const responseBody = {
      statusCode: httpStatus,
      errorType: camelToSnakeCase(`${exception.constructor.name}`),
      path: httpAdapter.getRequestUrl(ctx.getRequest()),
      details: validationErrors,
    };

    httpAdapter.reply(ctx.getResponse(), responseBody, httpStatus);
  }
}
