export enum Environment {
  Production = 'production',
  Development = 'development',
  VSCodeDebug = 'VSCodeDebug',
}
