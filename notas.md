typeorm-model-generator -h localhost -p 5432 -d db_probox -u admin -x str0ng+passw0rd -e postgres -o ./src/common/entities -s public --noConfig

# For specific table

typeorm-model-generator -h localhost -p 5432 -d db_probox -u admin -x str0ng+passw0rd -e postgres -o ./src/common/entities -s public --noConfig --tables "testing"
